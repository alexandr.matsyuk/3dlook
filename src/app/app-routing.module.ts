import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { OrdersComponent } from './views/orders/orders.component';
import { OrdersDetailsComponent } from './views/orders-details/orders-details.component';
import { ScannedUsersComponent } from './views/scanned-users/scanned-users.component';
import { BodyDataAnalyticsComponent } from './views/body-data-analytics/body-data-analytics.component';
import { CustomersDetailsComponent } from './views/customers-details/customers-details.component';
import { UsersDetailsComponent } from './views/users-details/users-details.component';

const routes: Routes = [
  { path: 'body-data-analytics', component: BodyDataAnalyticsComponent },
  { path: 'customers/details', component: CustomersDetailsComponent },
  { path: 'scanned-users/details', component: UsersDetailsComponent },
  { path: 'scanned-users', component: ScannedUsersComponent },
  { path: 'orders/details', component: OrdersDetailsComponent },
  { path: 'orders', component: OrdersComponent },

  { path: '**', component: DashboardComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
