import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartsModule } from 'ng2-charts';
import { ChartComponent } from './components/chart/chart.component';
import { LayoutComponent } from './components/layout/layout.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderPageComponent } from './components/header-page/header-page.component';
import { HeaderLayoutComponent } from './components/header-layout/header-layout.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { NavItemComponent } from './components/nav-item/nav-item.component';
import { ChartResultComponent } from './components/chart/chart-result/chart-result.component';
import { TableComponent } from './components/table/table.component';
import { SizeComponent } from './components/size/size.component';
import { OrdersComponent } from './views/orders/orders.component';
import { StatusComponent } from './components/status/status.component';
import { PurchasedSizeComponent } from './components/purchased-size/purchased-size.component';
import { FooterLayoutPaginationComponent } from './components/footer-layout-pagination/footer-layout-pagination.component';
import { OrdersDetailsComponent } from './views/orders-details/orders-details.component';
import { InfoColumnsComponent } from './components/info-columns/info-columns.component';
import { InfoItemComponent } from './components/info-item/info-item.component';
import { OrderItemComponent } from './components/order-item/order-item.component';
import { HeaderSingleComponent } from './components/header-single/header-single.component';
import { SubheaderLayoutComponent } from './components/subheader-layout/subheader-layout.component';
import { StatusNoOutlineComponent } from './components/status-no-outline/status-no-outline.component';
import { ScannedUsersComponent } from './views/scanned-users/scanned-users.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { InputComponent } from './components/input/input.component';
import { ButtonComponent } from './components/button/button.component';
import { CustomersDetailsComponent } from './views/customers-details/customers-details.component';
import { UsersDetailsComponent } from './views/users-details/users-details.component';
import { BodyDataAnalyticsComponent } from './views/body-data-analytics/body-data-analytics.component';
import { InfoColumnsRowComponent } from './components/info-columns-row/info-columns-row.component';
import { ChooseComponent } from './components/choose/choose.component';
import { ItemDetailPopupComponent } from './components/item-detail-popup/item-detail-popup.component';
import { InfoColumnsRowItemComponent } from './components/info-columns-row-item/info-columns-row-item.component';
import { SelectComponent } from './components/select/select.component';
import { DropdownColorComponent } from './components/dropdown-color/dropdown-color.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { FormsModule } from '@angular/forms';
import { BodyShapeComponent } from './components/body-shape/body-shape.component';
import { BodyShapeWithTitleComponent } from './components/body-shape-with-title/body-shape-with-title.component';
import { RangeComponent } from './components/range/range.component';
import { Ng5SliderModule } from 'ng5-slider';

@NgModule({
  declarations: [
    AppComponent,
    ChartComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    HeaderPageComponent,
    HeaderLayoutComponent,
    DashboardComponent,
    NavItemComponent,
    ChartResultComponent,
    TableComponent,
    SizeComponent,
    OrdersComponent,
    StatusComponent,
    PurchasedSizeComponent,
    FooterLayoutPaginationComponent,
    OrdersDetailsComponent,
    InfoColumnsComponent,
    InfoItemComponent,
    OrderItemComponent,
    HeaderSingleComponent,
    SubheaderLayoutComponent,
    StatusNoOutlineComponent,
    ScannedUsersComponent,
    PaginationComponent,
    DropdownComponent,
    InputComponent,
    ButtonComponent,
    CustomersDetailsComponent,
    UsersDetailsComponent,
    BodyDataAnalyticsComponent,
    InfoColumnsRowComponent,
    ChooseComponent,
    ItemDetailPopupComponent,
    InfoColumnsRowItemComponent,
    SelectComponent,
    DropdownColorComponent,
    DatePickerComponent,
    BodyShapeComponent,
    BodyShapeWithTitleComponent,
    RangeComponent,
    // ChartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    NgbModule,
    FormsModule,
    Ng5SliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
