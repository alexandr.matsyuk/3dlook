import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyShapeWithTitleComponent } from './body-shape-with-title.component';

describe('BodyShapeWithTitleComponent', () => {
  let component: BodyShapeWithTitleComponent;
  let fixture: ComponentFixture<BodyShapeWithTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyShapeWithTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyShapeWithTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
