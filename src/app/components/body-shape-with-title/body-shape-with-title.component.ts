import { Component, OnInit, Input } from '@angular/core';
import { Shape, BodyShape } from '../body-shape/body-shape.component';

@Component({
  selector: 'app-body-shape-with-title',
  templateUrl: './body-shape-with-title.component.html',
  styleUrls: ['./body-shape-with-title.component.scss']
})
export class BodyShapeWithTitleComponent implements OnInit {
  @Input() title;
  @Input() bodyShapesList: Array<BodyShape>;
  constructor() { }

  ngOnInit() {
  }

}
