import { Component, OnInit, Input } from '@angular/core';

export interface Shape {
  value: string;
  title: string;
  subtitle?: string;

}

export interface BodyShape {
  img: string;
  shapes: Array<Shape>;
}

@Component({
  selector: 'app-body-shape',
  templateUrl: './body-shape.component.html',
  styleUrls: ['./body-shape.component.scss']
})
export class BodyShapeComponent implements OnInit {
  @Input() bodyShapes: BodyShape;

  constructor() { }

  ngOnInit() {
  }

}
