import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() color = 'black';
  @Input() type;
  @Input() styles;
  isOpen;
  constructor() { }

  ngOnInit() {
  }

  toggleDropdown() {
    console.log('toggle');
    this.isOpen = !this.isOpen;
  }
}
