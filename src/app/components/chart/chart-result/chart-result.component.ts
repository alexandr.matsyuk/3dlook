import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-chart-result',
  templateUrl: './chart-result.component.html',
  styleUrls: ['./chart-result.component.scss']
})
export class ChartResultComponent implements OnInit {
  @Input() title;
  @Input() description;
  @Input() value;
  @Input() isLow;

  constructor() { }

  ngOnInit() {
  }

}
