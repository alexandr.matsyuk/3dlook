import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import * as Chart from 'chart.js';

Chart.defaults.global.defaultFontFamily = 'Barlow';



Chart.Tooltip.positioners.custom = function(elements, eventPosition) {
  /** @type {Chart.Tooltip} */
  const tooltip = this;

  return { x: eventPosition.x, y: 0 };

};


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  @Input() colors;
  @Input() results;
  @Input() inputlineChartData;

  public lineChartData: ChartDataSets[];

  public lineChartLabels: Label[] = [
    ['Mon', '16 Sep'],
    '|',
    ['Tue', '18 Sep'],
    '|',
    ['Wed', '19 Sep'],
    '|',
    ['Thu', '20 Sep'],
    '|',
    ['Fri', '21 Sep'],
    '|',
    ['Sat', '22 Sep'],
    '|',
    ['Sun', '23 Sep']
  ];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    elements: { line: { tension: 0 } },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
        gridLines: {
          display: false,
          zeroLineColor: 'transparent',
          drawBorder: false,
        },
        ticks: {
          beginAtZero: true,
          fontColor: '#A4AAC7',
          backdropColor:  '#A4AAC7',
          padding: 10,
          fontSize: 12,
          fontFamily: 'Barlow',
          fontStyle: 'bold',
        },
        scaleLabel: {
          fontColor: 'red'
        }
      }],

      yAxes: [
        {
          display: true,

          gridLines: {
            color: '#E4E5EC',
            zeroLineColor: 'transparent',
            drawBorder: false,
          },
          ticks: {
            beginAtZero: true,
            max: 4000,
            stepSize: 1000,
            fontColor: '#A4AAC7' ,
            backdropColor: '#A4AAC7' ,
            padding: 20,
            fontSize: 12,
            fontFamily: 'Barlow'
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',

          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'Tue,18 Sep',
          borderDash: [2, 2],
          borderColor: this.colors && this.colors[1] ,
          borderWidth: 2,
        },
      ],
    },
    tooltips: {
      mode: 'index',
      intersect: false,
      titleFontColor: 'transparent',
      titleFontSize: 0,
      position: 'custom',
      backgroundColor: '#202129',

      custom: (tooltipModel) => {
        console.log(tooltipModel);
        tooltipModel.cornerRadius = 3;
        tooltipModel.yAlign = 'left';
        tooltipModel.width = 100;
        tooltipModel._bodyFontStyle = 'bold';
        // tooltipModel.a
      },
      //   titleFontColor: 'transparent',
      //   titleFontSize: 0,
      //   bodyFontStyle: 'bold',
      //   bodyAlign: 'left',
      //   xPadding: 6,
      //   yPadding: 6,
      //   // yAlign: 'top',
      //   displayColors: false,
      callbacks: {
        label(tooltipItem, data) {
          console.log('tooltipItem', tooltipItem, data);
          return data.datasets[tooltipItem.datasetIndex].label;
        },
        //     beforeLabel(tooltipItem, data) {
        //       data.datasets[0].backgroundColor = '#fff';
        //       return {
        //         backdropColor: ''
        //       };
        //     },

        afterLabel(tooltipItem, data) {
          console.log('afterLabel', tooltipItem, data);

          // tooltipItem.
          // var dataset = data['datasets'][0];
          // var percent = Math.round((dataset['data'][tooltipItem['index']] / dataset["_meta"][0]['total']) * 100)
          return `${tooltipItem.yLabel}`;
        }
      }
    }
  };
  public lineChartColors: Color[] ;
  public lineChartLegend = false;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];
  public lineTension = 0;

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  constructor() { }

  ngOnInit() {
    console.log(this.inputlineChartData);
    this.lineChartColors = [
      {
        backgroundColor: `${this.colors[0]}33` ,
        borderColor: this.colors[0] ,
        pointBackgroundColor: this.colors[0] ,
        pointBorderColor: 'transparent',
        pointHoverBackgroundColor: this.colors[0],
        pointHoverBorderColor: this.colors[0] ,
        pointHoverRadius: 5,
      },
      {
        backgroundColor: this.colors && `${this.colors[1]}33`,
        borderColor: this.colors && this.colors[1] ,
        pointBackgroundColor: this.colors && this.colors[1],
        pointBorderColor: 'transparent',
        pointHoverBackgroundColor: this.colors && this.colors[1] ,
        pointHoverBorderColor: this.colors && this.colors[1] ,
        pointHoverRadius: 5
      },
    ];
    this.lineChartData = this.inputlineChartData;
  }

  public randomize(): void {
    for (let i = 0; i < this.lineChartData.length; i++) {
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        this.lineChartData[i].data[j] = this.generateNumber(i);
      }
    }
    this.chart.update();
  }

  private generateNumber(i: number) {
    return Math.floor((Math.random() * (i < 2 ? 100 : 1000)) + 1);
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {

  }

  public hideOne() {
    const isHidden = this.chart.isDatasetHidden(1);
    this.chart.hideDataset(1, !isHidden);
  }

  public pushOne() {
    this.lineChartData.forEach((x, i) => {
      const num = this.generateNumber(i);
      const data: number[] = x.data as number[];
      data.push(num);
    });
    this.lineChartLabels.push(`Label ${this.lineChartLabels.length}`);
  }

  public changeColor() {
    this.lineChartColors[2].borderColor = 'green';
    this.lineChartColors[2].backgroundColor = `rgba(0, 255, 0, 0.3)`;
  }

  public changeLabel() {
    this.lineChartLabels[2] = ['1st Line', '2nd Line'];
    // this.chart.update();
  }
}
