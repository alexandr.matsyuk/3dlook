import { Component, OnInit, Input } from '@angular/core';

export interface Variant {
  id: number;
  title: string;
}

@Component({
  selector: 'app-choose',
  templateUrl: './choose.component.html',
  styleUrls: ['./choose.component.scss']
})
export class ChooseComponent implements OnInit {
  @Input() variants: Array<Variant>;
  currentVariant: number;
  @Input() styles;
  constructor() { }

  ngOnInit() {
    this.currentVariant = this.variants[this.variants.length - 1].id;
  }

  makeChoice(e) {
    this.currentVariant = e;
  }

}
