import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dropdown-color',
  templateUrl: './dropdown-color.component.html',
  styleUrls: ['./dropdown-color.component.scss']
})
export class DropdownColorComponent implements OnInit {
  @Input() list;
  isOpen;
  current;
  constructor() {

  }

  ngOnInit() {
    this.current = this.list[0];
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  closeDropdown() {
    this.isOpen = false;
  }

  chooseItem(e: Event) {
    console.log(e);
    this.current = this.list.find(elem => elem.value === e);
    this.isOpen = !this.isOpen;
  }
}
