import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  @Input() list;
  @Input() isContinue = false;
  @Input() styles;
  @Input() isCustomItems;
  isOpen;
  current;
  constructor() {

  }

  ngOnInit() {
    this.current = this.list[0].value || this.list[0];
    console.log(this.styles);
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  chooseItem(e: Event) {
    console.log(e);
    this.current = e;
    this.isOpen = !this.isOpen;
  }

}
