import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterLayoutPaginationComponent } from './footer-layout-pagination.component';

describe('FooterLayoutPaginationComponent', () => {
  let component: FooterLayoutPaginationComponent;
  let fixture: ComponentFixture<FooterLayoutPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterLayoutPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterLayoutPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
