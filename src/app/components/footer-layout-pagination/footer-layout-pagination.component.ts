import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-footer-layout-pagination',
  templateUrl: './footer-layout-pagination.component.html',
  styleUrls: ['./footer-layout-pagination.component.scss']
})
export class FooterLayoutPaginationComponent implements OnInit {
  @Input() pageCount;

  constructor() { }

  ngOnInit() {
  }

}
