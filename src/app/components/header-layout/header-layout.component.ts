import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header-layout',
  templateUrl: './header-layout.component.html',
  styleUrls: ['./header-layout.component.scss']
})
export class HeaderLayoutComponent implements OnInit {
  @Input() styles;
  constructor() { }

  ngOnInit() {
  }

}
