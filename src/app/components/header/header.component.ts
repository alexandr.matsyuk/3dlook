import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // this.route = window.location.pathname;
    this.router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationEnd) {
        console.log('MY FAVORITE URL.... 1XBet', event.url);
      }
  });
  }

}
