import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoColumnsRowItemComponent } from './info-columns-row-item.component';

describe('InfoColumnsRowItemComponent', () => {
  let component: InfoColumnsRowItemComponent;
  let fixture: ComponentFixture<InfoColumnsRowItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoColumnsRowItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoColumnsRowItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
