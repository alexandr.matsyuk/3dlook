import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-columns-row-item',
  templateUrl: './info-columns-row-item.component.html',
  styleUrls: ['./info-columns-row-item.component.scss']
})
export class InfoColumnsRowItemComponent implements OnInit {
  @Input() item;
  isOpenPopup;
  side = 'right';
  constructor() { }

  ngOnInit() {
  }

  openPopup(e) {
    console.log(window.innerWidth, e.clientX, window.innerWidth - ((30 * window.innerWidth) / 100));
    if (e.clientX > window.innerWidth - ((40 * window.innerWidth) / 100)) {
      this.side = 'left';
    } else {
      this.side = 'right';
    }
    this.isOpenPopup = true;
  }

  closePopup(e) {
    this.isOpenPopup = false;
  }
}
