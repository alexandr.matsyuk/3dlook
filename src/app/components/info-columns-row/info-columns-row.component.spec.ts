import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoColumnsRowComponent } from './info-columns-row.component';

describe('InfoColumnsRowComponent', () => {
  let component: InfoColumnsRowComponent;
  let fixture: ComponentFixture<InfoColumnsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoColumnsRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoColumnsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
