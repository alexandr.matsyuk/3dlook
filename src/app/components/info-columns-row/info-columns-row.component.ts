import { Component, OnInit, Input } from '@angular/core';
import { DataItem } from '../info-columns/info-columns.component';

@Component({
  selector: 'app-info-columns-row',
  templateUrl: './info-columns-row.component.html',
  styleUrls: ['./info-columns-row.component.scss']
})
export class InfoColumnsRowComponent implements OnInit {
  @Input() title;
  @Input() rowsCount;
  @Input() columnsCount;
  @Input() dataList: Array<DataItem>;
  @Input() variants;
  isOpenPopup;

  constructor() { }

  ngOnInit() {
  }



}
