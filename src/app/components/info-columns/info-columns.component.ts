import { Component, OnInit, Input } from '@angular/core';

export interface DataItem {
  title: string;
  value: string | object;
  isCentered?: boolean;
  stylesInfoItem?: object;
  details?: object;
}

@Component({
  selector: 'app-info-columns',
  templateUrl: './info-columns.component.html',
  styleUrls: ['./info-columns.component.scss']
})
export class InfoColumnsComponent implements OnInit {
  @Input() columnsCount;
  @Input() rowsCount;
  @Input() dataList: Array<DataItem>;
  @Input() titleInfo;
  @Input() isLeftBorder;

  constructor() { }

  ngOnInit() {
  }

}
