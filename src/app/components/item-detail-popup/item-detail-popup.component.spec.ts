import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDetailPopupComponent } from './item-detail-popup.component';

describe('ItemDetailPopupComponent', () => {
  let component: ItemDetailPopupComponent;
  let fixture: ComponentFixture<ItemDetailPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemDetailPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDetailPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
