import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-item-detail-popup',
  templateUrl: './item-detail-popup.component.html',
  styleUrls: ['./item-detail-popup.component.scss']
})
export class ItemDetailPopupComponent implements OnInit {
  @Input() details;
  @Input() isOpen;
  @Input() side;
  @Input() title;
  constructor() { }

  ngOnInit() {
    console.log(this.side);
  }

}
