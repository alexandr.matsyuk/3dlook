import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() pages = 50;
  public currentPage: number;
  constructor() {

   }

  ngOnInit() {
    this.currentPage = 3;
    console.log(this.pages);
  }

  changePage(e) {
    console.log(e);
    this.currentPage = Number(e);
  }

}
