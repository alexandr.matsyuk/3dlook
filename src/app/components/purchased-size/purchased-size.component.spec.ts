import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasedSizeComponent } from './purchased-size.component';

describe('PurchasedSizeComponent', () => {
  let component: PurchasedSizeComponent;
  let fixture: ComponentFixture<PurchasedSizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasedSizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasedSizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
