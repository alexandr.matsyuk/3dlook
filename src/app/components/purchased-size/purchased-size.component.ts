import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-purchased-size',
  templateUrl: './purchased-size.component.html',
  styleUrls: ['./purchased-size.component.scss']
})
export class PurchasedSizeComponent implements OnInit {
  @Input() size;
  @Input() noCenter;
  constructor() { }

  ngOnInit() {
  }

}
