import { Component, OnInit } from '@angular/core';
import { Options, LabelType } from 'ng5-slider';

@Component({
  selector: 'app-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.scss']
})
export class RangeComponent implements OnInit {
  value = 5.1;
  highValue = 6.2;
  options: Options = {
    floor:  4.11,
    ceil: 7.2,
    // translate: (value: number, label: LabelType): string => {
    //   console.log(label);
    //   switch (label) {
    //     case LabelType.Low:
    //       return `${(Math.round(value * 7.2) / 100 + 4.11).toFixed(2)}`;
    //     case LabelType.High:
    //       return `${(Math.round(value * 7.2) / 100 + 4.11).toFixed(2)}`;
    //     default:
    //       return `${(Math.round(value * 7.2) / 100 + 4.11).toFixed(2)}`;
    //   }
    // }
  };
  constructor() { }

  ngOnInit() {
  }

}
