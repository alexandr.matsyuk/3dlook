import { Component, OnInit, Input } from '@angular/core';

export interface Option {
  label: string;
  value: string | boolean;
}

export interface Select {
  title: string;
  dataList: Array<Option>;
  type?: string;
}

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  @Input() selectItem: Select;
  isOpen;
  current;
  constructor() { }

  ngOnInit() {
    this.current = this.selectItem.dataList[0].value || this.selectItem.dataList[0];
  }
  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  chooseItem(e: Event) {
    console.log(e);
    this.current = e;
    this.isOpen = !this.isOpen;
  }

}
