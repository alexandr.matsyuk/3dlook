import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusNoOutlineComponent } from './status-no-outline.component';

describe('StatusNoOutlineComponent', () => {
  let component: StatusNoOutlineComponent;
  let fixture: ComponentFixture<StatusNoOutlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusNoOutlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusNoOutlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
