import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-status-no-outline',
  templateUrl: './status-no-outline.component.html',
  styleUrls: ['./status-no-outline.component.scss']
})
export class StatusNoOutlineComponent implements OnInit {
  @Input() status;
  constructor() {
    console.log(this.status);
  }

  ngOnInit() {
  }

}
