import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubheaderLayoutComponent } from './subheader-layout.component';

describe('SubheaderLayoutComponent', () => {
  let component: SubheaderLayoutComponent;
  let fixture: ComponentFixture<SubheaderLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubheaderLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubheaderLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
