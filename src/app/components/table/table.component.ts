import { Component, OnInit, Input } from '@angular/core';

export interface Column {
  title: string;
  key: string;
  isCentered?: boolean;
}

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() rows;
  @Input() columns: Array<Column>;
  objectKeys = Object.keys;

  public collapseOppened = new Set([]);

  constructor() { }

  ngOnInit() {
    // console.log(this.objectKeys(this.rows[1]));
  }

  public toggleCollapse(orderId) {
    console.log(orderId);

    if (this.collapseOppened.has(orderId)) {
      this.collapseOppened.delete(orderId);
    } else {
      this.collapseOppened.add(orderId);
    }
  }
}
