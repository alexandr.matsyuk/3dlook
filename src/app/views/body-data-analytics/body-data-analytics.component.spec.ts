import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyDataAnalyticsComponent } from './body-data-analytics.component';

describe('BodyDataAnalyticsComponent', () => {
  let component: BodyDataAnalyticsComponent;
  let fixture: ComponentFixture<BodyDataAnalyticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BodyDataAnalyticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyDataAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
