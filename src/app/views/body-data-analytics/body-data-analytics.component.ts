import { Component, OnInit } from '@angular/core';
import { Option, Select } from 'src/app/components/select/select.component';
import { Shape, BodyShape } from 'src/app/components/body-shape/body-shape.component';
import { Column } from 'src/app/components/table/table.component';
import { Variant } from 'src/app/components/choose/choose.component';



@Component({
  selector: 'app-body-data-analytics',
  templateUrl: './body-data-analytics.component.html',
  styleUrls: ['./body-data-analytics.component.scss']
})
export class BodyDataAnalyticsComponent implements OnInit {

  dataListMadeOrder: Array<Option> = [
    {
      label: 'Yes',
      value: 'Yes'
    },
    {
      label: 'No',
      value: 'No'
    }
  ];
  dataListMadeReturn: Array<Option> = [
    {
      label: 'Yes',
      value: 'Yes'
    },
    {
      label: 'No',
      value: 'No'
    }
  ];
  dataListCategory: Array<Option> = [
    {
      label: 'Maternity',
      value: 'Maternity'
    },
    {
      label: 'Maternity2',
      value: 'Maternity2'
    }
  ];

  dataListWeight: Array<Option> = [
    {
      label: '170-180 lb',
      value: '170-180 lb'
    },
    {
      label: '150-170 lb',
      value: '150-170 lb'
    }
  ];

  dataListCountry: Array<Option> = [
    {
      label: 'United States of America',
      value: 'United States of America'
    },
  ];
  dataListCity: Array<Option> = [
    {
      label: 'Denwer',
      value: 'Denwer'
    },
    {
      label: 'Denwer2',
      value: 'Denwer2'
    },
    {
      label: 'Denwer3',
      value: 'Denwer4'
    },
  ];

  dataListState: Array<Option> = [
    {
      label: 'CO',
      value: 'CO'
    },
    {
      label: 'CO2',
      value: 'CO2'
    },
    {
      label: 'CO3',
      value: 'CO3'
    },
  ];
  dataListSize: Array<Option> = [
    {
      label: '26',
      value: '26'
    },
    {
      label: '28',
      value: '28'
    }
  ];
  dataListPurchasedSize: Array<Option> = [
    {
      label: '24',
      value: '24'
    },
    {
      label: '26',
      value: '26'
    }
  ];

  madeOrder: Select = { dataList: this.dataListMadeOrder, title: 'Made an order', type: 'select' };
  madeReturn: Select = { dataList: this.dataListMadeReturn, title: 'Made a return', type: 'select' };
  category: Select = { dataList: this.dataListCategory, title: 'Category', type: 'select' };
  state: Select = { dataList: this.dataListState, title: 'State', type: 'select' };

  dataListOrders = {
    madeOrder: this.madeOrder,
    madeReturn: this.madeReturn,
    category: this.category
  };

  dataListParameters = {
    height: { type: 'text', title: 'Height' },
    weight: { dataList: this.dataListWeight, title: 'Weight', type: 'select' }
  };

  dataListLocation = {
    country: { dataList: this.dataListCountry, title: 'Country', type: 'select' },
    city: { dataList: this.dataListCity, title: 'City', type: 'select' },
    state: { dataList: this.dataListState, title: 'State', type: 'select' }
  };

  dataListSizeDetails = {
    size: { dataList: this.dataListSize, title: 'Recommended size', type: 'select' },
    purchasedSize: { dataList: this.dataListPurchasedSize, title: 'Purchased size', type: 'select' },

  };

  averageShapesList: Array<BodyShape> = [
    {
      img: '../../../assets/data_analytics/img_1.svg',
      shapes: [
        {
          value: '31"',
          title: 'chest'
        },
        {
          value: '24"',
          title: 'waist'
        },
        {
          value: '26"',
          title: 'hips'
        },
      ],
    },
  ];

  averageShapes = {
    title: 'Average Body Shape',
    shapeList: this.averageShapesList
  };

  insightsShapesList: Array<BodyShape> = [
    {
      img: '../../../assets/data_analytics/img_2.svg',
      shapes: [
        {
          value: '60%',
          title: 'of customers',
          subtitle: 'were recommened size 26'
        },
      ],
    },
    {
      img: '../../../assets/data_analytics/img_3.svg',
      shapes: [
        {
          value: '50%',
          title: 'of customers',
          subtitle: 'have hips/waist ratio 1.3'
        },
      ],
    },
    {
      img: '../../../assets/data_analytics/img_4.svg',
      shapes: [
        {
          value: '30%',
          title: 'of customers',
          subtitle: 'have average waist from 30” to 32”'
        },
      ],
    },
    {
      img: '../../../assets/data_analytics/img_5.svg',
      shapes: [
        {
          value: '28%',
          title: 'of customers',
          subtitle: 'have average waist from 28” to 30”'
        },
      ],
    },
  ];

  insightsShapes = {
    title: 'Insights',
    shapeList: this.insightsShapesList
  };

  hourglassShapesList: Array<BodyShape> = [{
    img: '../../../assets/data_analytics/img_6.svg',
    shapes: [
      {
        value: '36%',
        title: 'of all',
      },
      {
        value: '2 874',
        title: 'customers',
      },
    ],
  }
  ];
  rectangleShapesList: Array<BodyShape> = [
    {
      img: '../../../assets/data_analytics/img_7.svg',
      shapes: [
        {
          value: '21%',
          title: 'of all',
        },
        {
          value: '1 831',
          title: 'customers',
        },
      ],
    },
  ];
  appleShapesList: Array<BodyShape> = [
    {
      img: '../../../assets/data_analytics/img_8.svg',
      shapes: [
        {
          value: '18%',
          title: 'of all',
        },
        {
          value: '1 210',
          title: 'customers',
        },
      ],
    },
  ];
  triangleShapesList: Array<BodyShape> = [
    {
      img: '../../../assets/data_analytics/img_9.svg',
      shapes: [
        {
          value: '15%',
          title: 'of all',
        },
        {
          value: '1 009',
          title: 'customers',
        },
      ],
    },
  ];
  invertedTriangleShapesList: Array<BodyShape> = [
    {
      img: '../../../assets/data_analytics/img_10.svg',
      shapes: [
        {
          value: '10%',
          title: 'of all',
        },
        {
          value: '812',
          title: 'customers',
        },
      ],
    },
  ];

  bodyTypes = {
    hourglass: {
      title: 'Hourglass',
      shapeList: this.hourglassShapesList
    },
    rectangle: {
      title: 'Rectangle',
      shapeList: this.rectangleShapesList
    },
    apple: {
      title: 'Apple',
      shapeList: this.appleShapesList
    },
    triangle: {
      title: 'Triangle',
      shapeList: this.triangleShapesList
    },
    invertedTriangle: {
      title: 'Inverted Triangle',
      shapeList: this.invertedTriangleShapesList
    }
  };

  tableColumns: Array<Column> = [
    {
      title: 'Name',
      key: 'name'
    },
    {
      title: 'Email',
      key: 'email'
    },
    {
      title: 'Height',
      key: 'height'
    },
    {
      title: 'Body Shape',
      key: 'bodyShape'
    },
    {
      title: 'Country',
      key: 'country'
    },
    {
      title: 'State',
      key: 'state'
    },
    {
      title: 'City',
      key: 'city'
    },
    {
      title: 'Made on order',
      key: 'madeOrder',
      isCentered: true
    },
    {
      title: 'Made a return:',
      key: 'madeReturn',
      isCentered: true
    },
    {
      title: 'Recommended size',
      key: 'size',
      isCentered: true
    },
    {
      title: 'Purchased size',
      key: 'purchasedSize',
      isCentered: true
    },
  ];

  tableRows = [
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },
    {
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      height: `5' 8"`,
      bodyShape: 'Hourglass',
      country: 'USA',
      state: 'CO',
      city: 'Denwer',
      madeOrder: 'Yes',
      madeReturn: { value: 'No', type: 'status-no-outline' },
      size: { value: 26, type: 'size' },
      purchasedSize: { value: 24, type: 'purchasedSize' }
    },

  ];

  variants: Array<Variant> = [

    {
      id: 2,
      title: 'Metric'
    },
    {
      id: 1,
      title: 'Imperial'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
