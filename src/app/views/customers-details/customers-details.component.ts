import { Component, OnInit } from '@angular/core';
import { DataItem } from 'src/app/components/info-columns/info-columns.component';
import { Column } from 'src/app/components/table/table.component';

@Component({
  selector: 'app-customers-details',
  templateUrl: './customers-details.component.html',
  styleUrls: ['./customers-details.component.scss']
})
export class CustomersDetailsComponent implements OnInit {

  generalInfo: Array<DataItem> = [
    {
      title: 'Name',
      value: 'Victoria Fernandez'
    },
    {
      title: 'Email',
      value: 'janet_fahey@smith.name'
    },
    {
      title: 'Shopify user ID',
      value: '87394260209',
      stylesInfoItem: { 'margin-left': '30%' }
    },
    {
      title: 'Phone number',
      value: '434-570-8523'
    },
    {
      title: 'Country',
      value: 'United States of America'
    },
    {
      title: 'User ID',
      value: '60209',
      stylesInfoItem: { 'margin-left': '30%' }
    },
  ];

  bodylInfo: Array<DataItem> = [
    {
      title: 'Gender',
      value: 'Female'
    },
    {
      title: 'Body shape',
      value: 'Triangle'
    },
    {
      title: 'Height',
      value: '183 cm'
    },
    {
      title: 'Weight',
      value: '120-129 LB'
    },
  ];

  activity: Array<DataItem> = [
    {
      title: 'Last scan date',
      value: '06 Mar 2019 at 08:24pm'
    },
    {
      title: 'Purchases',
      value: 'Yes  (3 times)'
    },
  ];

  volumetricList: Array<DataItem> = [
    {
      title: 'Waist girth',
      value: '67 cm'
    },
    {
      title: 'Knee girth',
      value: '83 cm'
    },
    {
      title: 'Thigh girth',
      value: '25 cm'
    },
    {
      title: 'Upper hip girth',
      value: '98 cm'
    },
    {
      title: 'Ankle girth',
      value: '21 cm'
    },
    {
      title: 'Hip girth',
      value: '24 cm'
    },
    {
      title: 'Calf girth',
      value: '53 cm'
    },
  ];

  linearList: Array<DataItem> = [
    {
      title: 'Outside leg length',
      value: '167 cm'
    },
    {
      title: 'Inside leg length',
      value: '76 cm'
    },
    {
      title: 'Straight body rise',
      value: '27 cm'
    },
    {
      title: 'Upper hip breadth',
      value: '98 cm'
    },
    {
      title: 'Waist to hip length',
      value: '32 cm'
    },
    {
      title: 'Waist to knee length',
      value: '78 cm'
    },
    {
      title: 'Side waist level to ankles',
      value: '54 cm'
    },
    {
      title: 'Hip height',
      value: '167 cm'
    },
    {
      title: 'Knee height',
      value: '90 cm'
    },
    {
      title: 'Outer ankle height',
      value: '67 cm'
    },
    {
      title: 'Waist Height',
      value: '84 cm'
    },
    {
      title: 'Upper hip height',
      value: '123 cm'
    },
  ];

  customersOrdersColumns: Array<Column> = [
    { title: 'Purchased ID', key: 'purchasedid' },
    { title: 'Order’s date', key: 'date' },
    { title: 'Category', key: 'collection' },
    { title: 'Product', key: 'product' },
    { title: 'Recommended Size', key: 'size', isCentered: true },
    { title: 'Purchased Size', key: 'purchasedSize', isCentered: true },
    { title: 'Refund', key: 'status', isCentered: true },
  ];

  customersOrdersRows = [
    {
      purchasedid: '#67234034',
      date: '15 Oct 2019',
      collection: 'Relaxed',
      product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans Iaaaaaaaaa', maxWidth: 130 },
      size: { type: 'size', value: 36 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'No' },
    },
    {
      purchasedid: '#08273134',
      date: '06 Mar 2019',
      collection: 'Skinny',
      product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130 },
      size: { type: 'size', value: 34 },
      purchasedSize: { type: 'purchasedSize', value: 34 },
      status: { type: 'status', value: 'No' },
    },
    {
      purchasedid: '#02322354',
      date: '10 May 2019',
      collection: 'Jeggings',
      product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans Iaaaaaaaaa', maxWidth: 130 },
      size: { type: 'size', value: 38 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'Yes' },
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
