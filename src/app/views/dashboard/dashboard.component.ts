import { Component, OnInit } from '@angular/core';
import { Column } from 'src/app/components/table/table.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  tableColumns: Array<Column> = [
    { title: 'Scan date', key: 'date' },
    { title: 'Name', key: 'name' },
    { title: 'Email', key: 'email' },
    { title: 'Person ID', key: 'personId' },
    { title: 'Product link', key: 'productLink' },
    { title: 'Recommended size', key: 'size', isCentered: true },
    { title: '', key: 'nextButton' }
  ];
  tableData = [
    {
      date: '15 Oct 2019 at 3:46pm',
      name: 'Mamie Morrison',
      email: 'janet_fahey@smith.name',
      personId: { type: 'link', value: '#67234034' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 36 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '23 Feb 2019 at 3:46pm',
      name: 'Phoebe Brewer',
      email: 'leda.weimann@carter.tv',
      personId: { type: 'link', value: '#74392024' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 38 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '06 Mar 2019 at 3:46pm',
      name: '',
      email: '',
      personId: { type: 'link', value: '#08273134' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 36 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '10 May 2019 at 3:46pm',
      name: 'Mary Becker',
      email: 'freeda_pouros@beatty.io',
      personId: { type: 'link', value: '#67234034' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 34 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '08 May 2019 at 3:46pm',
      name: 'Sarah Gilbert',
      email: 'fadel_perry@shanahan.ca',
      personId: { type: 'link', value: '#34829064' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 40 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '25 Feb 2019 at 3:46pm',
      name: '',
      email: 'bailee_metz@alanis.com',
      personId: { type: 'link', value: '#67234034' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 36 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '22 Sep 2019 at 3:46pm',
      name: 'Victoria Fernandez',
      email: 'mraz.shania@gmail.com',
      personId: { type: 'link', value: '#03914654' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 38 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '28 Dec 2019 at 3:46pm',
      name: 'Tillie Walsh',
      email: '',
      personId: { type: 'link', value: '#34829064' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 34 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '01 Dec 2019 at 3:46pm',
      name: 'Amanda Peterson',
      email: '',
      personId: { type: 'link', value: '#02322354' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 38 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '22 Mar 2019 at 3:46pm',
      name: 'Abbie Watts',
      email: 'katlyn_farrell@waters.tv',
      personId: { type: 'link', value: '#03914654' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 34 },
      nextButton: { type: 'nextButton', value: true }
    },
  ];

  lineChartData = [
    { data: [500, 1500, 100, 2100, 1700, 1800, 1500, 2600, 3000, 2700, 3200, 3000, 3500], label: 'Visitors', },
    { data: [300, 1300, 1600, 2000, 1400, 1400, 1200, 2300, 3000, 2500, 3000, 3300, 3200], label: 'Orders' },
  ];

  results = [
    {
      title: 'Visitors',
      value: '2 867',
    },
    {
      title: 'Orders',
      value: '1 583',
      isLow: true
    },
    {
      title: 'Scanned customers',
      value: '2 250'
    },
    {
      title: 'Orders after size recommendation',
      value: '1 498',
    },
    {
      title: 'Widget conversion',
      value: '3,7%'
    },
    {
      title: 'Conversion after widget flow',
      value: '1,4%',
      isLow: true
    },
  ];

  constructor() { }

  ngOnInit() { }
}
