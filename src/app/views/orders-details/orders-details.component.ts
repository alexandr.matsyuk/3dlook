import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orders-details',
  templateUrl: './orders-details.component.html',
  styleUrls: ['./orders-details.component.scss']
})
export class OrdersDetailsComponent implements OnInit {
  generalInfo = [
    {
      title: 'Order ID',
      value: '67234034'
    },
    {
      title: 'Items in orders',
      value: '3 items'
    },
    {
      title: 'Order’s date',
      value: '15 Oct 2019'
    },
    {
      title: 'Refunds',
      value: { type: 'status', value: 'Yes' }
    }
  ];

  customersInfo = [
    {
      title: 'Name',
      value: 'Victoria Fernandez'
    },
    {
      title: 'Email:',
      value: 'janet_fahey@smith.name'
    },
    {
      title: 'Shopify user ID',
      value: '87394260209',
      isCentered: true
    },
    {
      title: 'Phone number',
      value: '434-570-8523'
    },
    {
      title: 'Country',
      value: 'United States of America'
    },
    {
      title: 'Last person ID',
      value: '60209',
      isCentered: true
    },
  ];

  item = {
    orderId: { type: 'link', value: '#03914654' },
    date: '22 Sep 2019',
    list: [
      {
        collection: 'Relaxed',
        product: { type: 'link', symbol: 'after', value: 'High Riѕе Buttеr Anklе Jeans In Wynter Blасk', maxWidth: 130 },
        size: { type: 'size', value: 36 },
        purchasedSize: { type: 'purchasedSize', value: 36 },
        status: { type: 'status', value: 'No' },
        cost: '39.00',
        img: '../../../assets/order_details/img_1.png'
      },
      {
        collection: 'Skinny',
        product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans In Wynter Lennox', maxWidth: 130 },
        size: { type: 'size', value: 34 },
        purchasedSize: { type: 'purchasedSize', value: 34 },
        status: { type: 'status', value: 'No' },
        cost: '59.00',
        img: '../../../assets/order_details/img_2.png'
      },
      {
        collection: 'Jeggings',
        product: { type: 'link', symbol: 'after', value: 'High Riѕе Buttеr Anklе Jeans In Wynter Blасk', maxWidth: 130 },
        size: { type: 'size', value: 38 },
        purchasedSize: { type: 'purchasedSize', value: 36 },
        status: { type: 'status', value: 'Yes' },
        cost: '39.00',
        img: '../../../assets/order_details/img_1.png'
      },
    ],
    nextButton: { type: 'nextButton' }
  };
  constructor() { }

  ngOnInit() {
  }

}
