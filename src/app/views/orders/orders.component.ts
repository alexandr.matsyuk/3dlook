import { Component, OnInit } from '@angular/core';
import { Column } from 'src/app/components/table/table.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  tableColumns: Array<Column> = [
    { title: 'Order ID', key: 'orderId' },
    { title: 'Order’s date', key: 'date' },
    { title: 'Collection', key: 'collection' },
    { title: 'Product', key: 'product' },
    { title: 'Recommended Size', key: 'size', isCentered: true },
    { title: 'Purchased Size', key: 'purchasedSize', isCentered: true },
    { title: 'Return status', key: 'status', isCentered: true },
    { title: '', key: 'nextButton' }
  ];
  tableData = [
    {
      orderId: { type: 'link', value: '#67234034', link: 'details' },
      date: '15 Oct 2019',
      collection: 'Jeggings',
      product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 36 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'No' }
    },
    {
      orderId: { type: 'link', value: '#74392024', link: 'details' },
      date: '23 Feb 2019',
      list: [
        {
          collection: 'Skinny',
          product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
          size: { type: 'size', value: 36 },
          purchasedSize: { type: 'purchasedSize', value: 36 },
          status: { type: 'status', value: 'No' },
        },
        {
          collection: 'Jeggings',
          product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
          size: { type: 'size', value: 36 },
          purchasedSize: { type: 'purchasedSize', value: 36 },
          status: { type: 'status', value: 'No' },
        },
      ],
      nextButton: { type: 'nextButton' }
    },
    {
      orderId: { type: 'link', value: '#08273134', link: 'details' },
      date: '06 Mar 2019',
      collection: 'Jeggings',
      product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans Iaaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 36 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'Yes' }
    },
    {
      orderId: { type: 'link', value: '#02322354', link: 'details' },
      date: '10 May 2019',
      collection: 'Flares',
      product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 34 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'No' }
    },
    {
      orderId: { type: 'link', value: '#34829064', link: 'details' },
      date: '08 May 2019',
      collection: 'Straight',
      product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans Iaaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 40 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'No' }
    },
    {
      orderId: { type: 'link', value: '#67234034', link: 'details' },
      date: '25 Feb 2019',
      collection: 'Jeggings',
      product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 36 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'No' }
    },
    {
      orderId: { type: 'link', value: '#03914654', link: 'details' },
      date: '22 Sep 2019',
      list: [
        {
          collection: 'Relaxed',
          product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans Iaaaaaaaaa', maxWidth: 130, link: 'details' },
          size: { type: 'size', value: 36 },
          purchasedSize: { type: 'purchasedSize', value: 36 },
          status: { type: 'status', value: 'No' },
        },
        {
          collection: 'Skinny',
          product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
          size: { type: 'size', value: 34 },
          purchasedSize: { type: 'purchasedSize', value: 34 },
          status: { type: 'status', value: 'No' },
        },
        {
          collection: 'Jeggings',
          product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans Iaaaaaaaaa', maxWidth: 130, link: 'details' },
          size: { type: 'size', value: 38 },
          purchasedSize: { type: 'purchasedSize', value: 36 },
          status: { type: 'status', value: 'Yes' },
        },
      ],
      nextButton: { type: 'nextButton' }
    },
    {
      orderId: { type: 'link', value: '#34829064', link: 'details' },
      date: '28 Dec 2019',
      collection: 'Skinny',
      product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 34 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'No' }
    },
    {
      orderId: { type: 'link', value: '#02322354', link: 'details' },
      date: '01 Dec 2019',
      collection: 'Jeggings',
      product: { type: 'link', symbol: 'after', value: 'Butter Skinny Jeans Iaaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 38 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'Yes' }
    },
    {
      orderId: { type: 'link', value: '#03914654', link: 'details' },
      date: '22 Mar 2019',
      collection: 'Flares',
      product: { type: 'link', symbol: 'after', value: 'High Rise Butter Ankle aaaaaaaaa', maxWidth: 130, link: 'details' },
      size: { type: 'size', value: 34 },
      purchasedSize: { type: 'purchasedSize', value: 36 },
      status: { type: 'status', value: 'No' }
    },
  ];
  inputlineChartData = [
    { data: [100, 1500, 100, 2100, 1700, 100, 1500, 2600, 3000, 700, 200, 3000, 3500], label: 'Scanned', },
    { data: [300, 1300, 1600, 2000, 800, 1400, 1200, 2300, 1000, 2500, 3000, 3300, 3200], label: 'Returns' },
  ];

  results = [
    {
      title: 'Scanned',
      value: '2 867',
    },
    {
      title: 'Returns',
      value: '1 583',
      isLow: true,
      description: '4 890 in $ amount'
    },
    {
      title: 'Orders total',
      value: '4 456',
      description: '4 890 in $ amount'
    },
    {
      title: 'Orders after widget flow',
      value: '1 498',
      description: '4 890 in $ amount'
    },
    {
      title: 'Returns after widget flow',
      value: '236',
      isLow: true,
      description: '4 890 in $ amount'
    },
  ];
  constructor() { }

  ngOnInit() {
  }

}
