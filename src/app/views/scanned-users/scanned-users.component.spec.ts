import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannedUsersComponent } from './scanned-users.component';

describe('ScannedUsersComponent', () => {
  let component: ScannedUsersComponent;
  let fixture: ComponentFixture<ScannedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScannedUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
