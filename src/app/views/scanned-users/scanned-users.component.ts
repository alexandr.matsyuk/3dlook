import { Component, OnInit } from '@angular/core';
import { Column } from 'src/app/components/table/table.component';

@Component({
  selector: 'app-scanned-users',
  templateUrl: './scanned-users.component.html',
  styleUrls: ['./scanned-users.component.scss']
})
export class ScannedUsersComponent implements OnInit {
  tableColumns: Array<Column> = [
    { title: 'Scan date', key: 'date' },
    { title: 'Email', key: 'email' },
    { title: 'Phone number', key: 'phone' },
    { title: 'Person ID', key: 'personId' },
    { title: 'Product link', key: 'productLink' },
    { title: 'Recommended size', key: 'size', isCentered: true },
    { title: '', key: 'nextButton' }
  ];
  tableData = [
    {
      date: '15 Oct 2019 at 3:46pm',
      phone: '426-658-3923',
      email: 'janet_fahey@smith.name',
      personId: { type: 'link', value: '#67234034', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 36 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '23 Feb 2019 at 3:46pm',
      phone: '872-526-8916',
      email: 'leda.weimann@carter.tv',
      personId: { type: 'link', value: '#74392024', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 38 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '06 Mar 2019 at 3:46pm',
      phone: '344-108-1618',
      email: '',
      personId: { type: 'link', value: '#08273134', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 36 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '10 May 2019 at 3:46pm',
      phone: '767-691-7853',
      email: 'freeda_pouros@beatty.io',
      personId: { type: 'link', value: '#67234034', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 34 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '08 May 2019 at 3:46pm',
      phone: '767-691-7853',
      email: 'fadel_perry@shanahan.ca',
      personId: { type: 'link', value: '#34829064', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 40 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '25 Feb 2019 at 3:46pm',
      phone: '733-965-0200',
      email: 'bailee_metz@alanis.com',
      personId: { type: 'link', value: '#67234034', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 36 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '22 Sep 2019 at 3:46pm',
      phone: '807-413-0026',
      email: 'mraz.shania@gmail.com',
      personId: { type: 'link', value: '#03914654', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 38 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '28 Dec 2019 at 3:46pm',
      phone: '277-860-2860',
      email: '',
      personId: { type: 'link', value: '#34829064', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 34 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '01 Dec 2019 at 3:46pm',
      phone: '588-076-8710',
      email: '',
      personId: { type: 'link', value: '#02322354', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 38 },
      nextButton: { type: 'nextButton', value: true }
    },
    {
      date: '22 Mar 2019 at 3:46pm',
      phone: '462-903-8892',
      email: 'katlyn_farrell@waters.tv',
      personId: { type: 'link', value: '#03914654', link: 'details' },
      productLink: { type: 'link', icon: 'before', value: '1822denim.com/collections/butter/productlink', symbol: 'before' },
      size: { type: 'size', value: 34 },
      nextButton: { type: 'nextButton', value: true }
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
