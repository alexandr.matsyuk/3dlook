import { Component, OnInit } from '@angular/core';
import { DataItem } from 'src/app/components/info-columns/info-columns.component';
import { Variant } from 'src/app/components/choose/choose.component';

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.scss']
})
export class UsersDetailsComponent implements OnInit {
  generalInfo: Array<DataItem> = [
    {
      title: 'Email',
      value: 'janet_fahey@smith.name'
    },
    {
      title: 'Country',
      value: 'United States of America'
    },
    {
      title: 'Person ID',
      value: '60209',
      stylesInfoItem: { 'margin-left': '30%' }
    },
    {
      title: 'Phone number',
      value: '434-570-8523',
      stylesInfoItem: { 'margin-bottom': '0' }
    },
    {
      title: 'Product link',
      value: {
        type: 'link',
        value: '1822denim.com/collaeaaaaa',
        symbol: 'before',
        maxWidth: 160
      },
      stylesInfoItem: { 'margin-bottom': '0' }
    },
    {
      title: 'Recommended size',
      value: {
        type: 'size',
        value: 36
      },
      stylesInfoItem: { 'margin-left': '30%', 'margin-bottom': '0' }
    }
  ];

  bodylInfo: Array<DataItem> = [
    {
      title: 'Gender',
      value: 'Female'
    },
    {
      title: 'Body shape',
      value: 'Triangle'
    },
    {
      title: 'Height',
      value: '183 cm'
    },
    {
      title: 'Weight',
      value: '120-129 LB'
    },
  ];

  activity: Array<DataItem> = [
    {
      title: 'Last scan date',
      value: '06 Mar 2019 at 08:24pm'
    },
  ];

  volumetricList: Array<DataItem> = [
    {
      title: 'Waist girth',
      value: '67 cm'
    },
    {
      title: 'Knee girth',
      value: '83 cm'
    },
    {
      title: 'Thigh girth',
      value: '25 cm'
    },
    {
      title: 'Upper hip girth',
      value: '98 cm'
    },
    {
      title: 'Ankle girth',
      value: '21 cm'
    },
    {
      title: 'Hip girth',
      value: '24 cm'
    },
    {
      title: 'Calf girth',
      value: '53 cm'
    },
    {
      title: 'Waist girth',
      value: '67 cm'
    },
    {
      title: 'Knee girth',
      value: '83 cm'
    },
    {
      title: 'Thigh girth',
      value: '25 cm'
    },
    {
      title: 'Upper hip girth',
      value: '98 cm'
    },
    {
      title: 'Ankle girth',
      value: '21 cm'
    },
    {
      title: 'Hip girth',
      value: '24 cm'
    },
    {
      title: 'Calf girth',
      value: '53 cm'
    },
    {
      title: 'Thigh girth',
      value: '25 cm'
    },
    {
      title: 'Upper hip girth',
      value: '98 cm'
    },
    {
      title: 'Ankle girth',
      value: '21 cm'
    },

  ];

  linearList: Array<DataItem> = [
    {
      title: 'Outside leg length',
      value: '167 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Inside leg length',
      value: '76 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Straight body rise',
      value: '27 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Upper hip breadth',
      value: '98 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Waist to hip length',
      value: '32 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Waist to knee length',
      value: '78 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Side waist level to ankles',
      value: '54 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Hip height',
      value: '167 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Knee height',
      value: '90 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Outer ankle height',
      value: '67 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Waist Height',
      value: '84 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Upper hip height',
      value: '123 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Outside leg length',
      value: '167 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Inside leg length',
      value: '76 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Straight body rise',
      value: '27 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Upper hip breadth',
      value: '98 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Waist to hip length',
      value: '32 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Waist to knee length',
      value: '78 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Side waist level to ankles',
      value: '54 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Hip height',
      value: '167 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Knee height',
      value: '90 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Outer ankle height',
      value: '67 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Waist Height',
      value: '84 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Upper hip height',
      value: '123 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Side waist level to ankles',
      value: '54 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Hip height',
      value: '167 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Knee height',
      value: '90 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Outer ankle height',
      value: '67 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
    {
      title: 'Waist Height',
      value: '84 cm',
      details: {
        definition: 'Vertical distance from the waist level to the hip level.',
        source: '3DLOOK',
        equipment: 'Sliding calliper'
      }
    },
  ];

  variants: Array<Variant> = [
    {
      id: 1,
      title: 'Imperial'
    },
    {
      id: 2,
      title: 'Metric'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
